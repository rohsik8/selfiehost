package com.selfie.host.ui.activities.splash.dagger;


import javax.inject.Scope;

@Scope
public @interface SplashScope {
}
