package com.selfie.host.ui.activities.login.dagger;

import javax.inject.Scope;

@Scope
public @interface LoginScope {
}
