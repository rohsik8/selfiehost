package com.selfie.host.ui.fragments.properties.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;

import com.selfie.host.R;
import com.selfie.host.databinding.AddNewPropertyBinding;

public class AddPropertyDialog {


    public AddNewPropertyBinding binding;
    Dialog dialog;
    Activity activity;


    public AddPropertyDialog(Activity activity) {
        this.activity = activity;
        dialog = new Dialog(activity, R.style.DialogStyle);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.add_new_property, null, false);
        dialog.setContentView(binding.getRoot());


    }


    public void showDialog(boolean doShow) {
        if (doShow) {
            // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        } else
            dialog.dismiss();
    }


}
