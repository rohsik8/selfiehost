package com.selfie.host.ui.activities.login.mvp;

import android.content.Intent;

import com.selfie.host.ui.activities.homepage.HomePageActivity;
import com.selfie.host.ui.activities.login.LoginParams;
import com.selfie.host.ui.activities.login.LoginResponse.LoginResponse;
import com.selfie.host.utils.Constants;

import java.io.IOException;
import java.net.SocketTimeoutException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

import static com.selfie.host.utils.GeneralUtils.getErrorMessage;

public class LoginPresenter {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private LoginView loginView;
    private LoginModel loginModel;

    public LoginPresenter(LoginView loginView, LoginModel loginModel) {
        this.loginView = loginView;
        this.loginModel = loginModel;
    }

    public void onCreateView() {

        loginView.binding.btnLogin.setOnClickListener(v -> {
            login();
        });

    }

    public void login() {
        loginView.showLoading(true);
        DisposableObserver<LoginResponse> disposableObserver = new DisposableObserver<LoginResponse>() {
            @Override
            public void onNext(LoginResponse response) {


                loginModel.preferencesManager.save(Constants.TOKEN, "Bearer " + response.getToken());

                //user
                loginModel.preferencesManager.save(Constants.FIRSTNAME, response.getUser().getFirstName());
                loginModel.preferencesManager.save(Constants.USERID, response.getUser().getId() + "");
                loginModel.preferencesManager.save(Constants.LASTNAME, response.getUser().getLastName());
                loginModel.preferencesManager.save(Constants.EMAIL, response.getUser().getEmail());
                //..............//

                //profile
                loginModel.preferencesManager.save(Constants.PHONE, response.getUser().getProfile().getPhone());
                loginModel.preferencesManager.save(Constants.ADDRESS, response.getUser().getProfile().getAddress() + "");
                loginModel.preferencesManager.save(Constants.CITY, response.getUser().getProfile().getCity());
                loginModel.preferencesManager.save(Constants.STATE, response.getUser().getProfile().getState());
                loginModel.preferencesManager.save(Constants.ZIP, response.getUser().getProfile().getZip());
                loginModel.preferencesManager.save(Constants.COUNTRY, response.getUser().getProfile().getCountry());
                //......//


                loginView.mActivity.startActivity(new Intent(loginView.mActivity, HomePageActivity.class));
                loginView.mActivity.finish();

            }

            @Override
            public void onError(Throwable e) {
                loginView.showLoading(false);
                if (e instanceof HttpException) {
                    ResponseBody responseBody = ((HttpException) e).response().errorBody();
                    loginView.showMessage(getErrorMessage(responseBody));

                } else if (e instanceof SocketTimeoutException) {
                    //newsView.showMessage("Time Out");
                } else if (e instanceof IOException) {
                    loginView.showMessage("Please check your network connection");

                } else {
                    loginView.showMessage(e.getMessage());
                }
            }

            @Override
            public void onComplete() {
                loginView.showLoading(false);
            }
        };
        loginModel.loginResponseObservable(loginParams())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(disposableObserver);
        compositeDisposable.add(disposableObserver);
    }


    public LoginParams loginParams() {
        return LoginParams.builder()
                .email(loginView.binding.edtEmail.getText().toString())
                .password(loginView.binding.edtPassword.getText().toString()).build();
    }


}

