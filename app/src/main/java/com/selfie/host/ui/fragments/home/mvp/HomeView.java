package com.selfie.host.ui.fragments.home.mvp;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.selfie.host.R;


@SuppressLint("ViewConstructor")
public class HomeView extends FrameLayout {

    AppCompatActivity activity;

    public HomeView(@NonNull AppCompatActivity activity) {
        super(activity);
        this.activity = activity;

        inflate(activity, R.layout.fragment_home, this);


    }


    public void showProgressDialog(boolean isLoading) {
        if (isLoading) {

        } else {

        }

    }


}
