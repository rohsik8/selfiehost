package com.selfie.host.ui.fragments.home.dagger;


import javax.inject.Scope;

@Scope
public @interface HomeScope {
}
