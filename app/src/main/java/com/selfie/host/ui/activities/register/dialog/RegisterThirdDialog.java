package com.selfie.host.ui.activities.register.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;

import com.selfie.host.R;
import com.selfie.host.databinding.RegisterLayoutThirdBinding;

public class RegisterThirdDialog {


    public RegisterLayoutThirdBinding binding;
    Dialog dialog;
    Activity activity;

    public RegisterThirdDialog(Activity activity) {
        this.activity = activity;
        dialog = new Dialog(activity, R.style.DialogStyle);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.register_layout_third, null, false);
        dialog.setContentView(binding.getRoot());


    }


    public void showDialog(boolean doShow) {
        if (doShow) {
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();

        } else
            dialog.dismiss();
    }

}
