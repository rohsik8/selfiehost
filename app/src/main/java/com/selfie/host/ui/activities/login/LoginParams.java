package com.selfie.host.ui.activities.login;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class LoginParams implements Parcelable {

    public static Builder builder() {
        return new $$AutoValue_LoginParams.Builder();
    }

    public static TypeAdapter<LoginParams> typeAdapter(Gson gson) {
        return new $AutoValue_LoginParams.GsonTypeAdapter(gson);
    }

    @SerializedName("email")
    public abstract String email();

    @SerializedName("password")
    public abstract String password();

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder email(String email);

        public abstract Builder password(String password);

        public abstract LoginParams build();
    }

}
