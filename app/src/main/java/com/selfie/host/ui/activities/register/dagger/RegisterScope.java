package com.selfie.host.ui.activities.register.dagger;

import javax.inject.Scope;

@Scope
public @interface RegisterScope {
}
