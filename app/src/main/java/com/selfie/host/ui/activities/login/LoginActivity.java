package com.selfie.host.ui.activities.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.selfie.host.application.dagger.AppApplication;
import com.selfie.host.ui.activities.login.dagger.DaggerLoginComponent;
import com.selfie.host.ui.activities.login.dagger.LoginModule;
import com.selfie.host.ui.activities.login.mvp.LoginPresenter;
import com.selfie.host.ui.activities.login.mvp.LoginView;

import javax.inject.Inject;

public class LoginActivity extends AppCompatActivity {
    @Inject
    LoginView loginView;

    @Inject
    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerLoginComponent.builder().appComponent(AppApplication.get(LoginActivity.this).appComponent())
                .loginModule(new LoginModule(this)).build().inject(this);
        //setContentView(loginView);
        loginPresenter.onCreateView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // loginPresenter.onDestroyView();
    }
}
