package com.selfie.host.ui.activities.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.selfie.host.application.dagger.AppApplication;
import com.selfie.host.ui.activities.register.dagger.DaggerRegisterComponent;
import com.selfie.host.ui.activities.register.dagger.RegisterModule;
import com.selfie.host.ui.activities.register.mvp.RegisterPresenter;
import com.selfie.host.ui.activities.register.mvp.RegisterView;

import javax.inject.Inject;

public class RegisterActivity extends AppCompatActivity {
    @Inject
    RegisterView registerView;

    @Inject
    RegisterPresenter registerPresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerRegisterComponent.builder().appComponent(AppApplication.get(RegisterActivity.this).appComponent())
                .registerModule(new RegisterModule(this)).build().inject(this);
        registerPresenter.onCreateView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // registerPresenter.onDestroyView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        registerPresenter.onActivityResult(requestCode, resultCode, data);
    }
}
