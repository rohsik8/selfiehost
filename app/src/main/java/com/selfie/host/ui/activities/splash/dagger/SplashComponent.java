package com.selfie.host.ui.activities.splash.dagger;


import com.selfie.host.application.dagger.AppComponent;
import com.selfie.host.ui.activities.splash.SplashActivity;

import dagger.Component;

@SplashScope
@Component(modules = SplashModule.class, dependencies = AppComponent.class)
public interface SplashComponent {

    void inject(SplashActivity splashActivity);
}
