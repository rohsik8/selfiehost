package com.selfie.host.ui.activities.login.mvp;


import com.selfie.host.application.AppNetwork;
import com.selfie.host.ext.storage.PreferencesManager;
import com.selfie.host.ui.activities.login.LoginParams;
import com.selfie.host.ui.activities.login.LoginResponse.LoginResponse;

import io.reactivex.Observable;

public class LoginModel {

    public PreferencesManager preferencesManager;
    private AppNetwork appNetwork;

    public LoginModel(AppNetwork padloktNetwork, PreferencesManager preferencesManager) {
        this.appNetwork = padloktNetwork;
        this.preferencesManager = preferencesManager;
    }

    public Observable<LoginResponse> loginResponseObservable(LoginParams loginParams) {
        return appNetwork.loginResponse(loginParams);
    }

}
