package com.selfie.host.ui.activities.register.mvp;


import com.selfie.host.application.AppNetwork;
import com.selfie.host.ext.storage.PreferencesManager;
import com.selfie.host.ui.activities.register.RegisterParams;
import com.selfie.host.ui.activities.register.RegisterResponse.RegisterResponse;

import io.reactivex.Observable;

public class RegisterModel {

    public PreferencesManager preferencesManager;
    private AppNetwork appNetwork;

    public RegisterModel(AppNetwork padloktNetwork, PreferencesManager preferencesManager) {
        this.appNetwork = padloktNetwork;
        this.preferencesManager = preferencesManager;
    }

    public Observable<RegisterResponse> registerResponseObservable(RegisterParams registerParams) {
        return appNetwork.registerResponse(registerParams);
    }

}
