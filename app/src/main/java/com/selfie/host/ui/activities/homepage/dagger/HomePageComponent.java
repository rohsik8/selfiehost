package com.selfie.host.ui.activities.homepage.dagger;


import com.selfie.host.application.dagger.AppComponent;
import com.selfie.host.ui.activities.homepage.HomePageActivity;

import dagger.Component;

@HomePageScope
@Component(modules = HomePageModule.class, dependencies = AppComponent.class)
public interface HomePageComponent {

    void inject(HomePageActivity homePageActivity);

}
