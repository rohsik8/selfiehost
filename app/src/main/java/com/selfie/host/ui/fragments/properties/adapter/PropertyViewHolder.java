package com.selfie.host.ui.fragments.properties.adapter;

import android.support.v7.widget.RecyclerView;

import com.selfie.host.databinding.PropertyListItemBinding;

public class PropertyViewHolder extends RecyclerView.ViewHolder {

    public final PropertyListItemBinding binding;

    public PropertyViewHolder(PropertyListItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

}