
package com.selfie.host.ui.fragments.properties.propertiesResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PropertyResponse implements Serializable {

    private final static long serialVersionUID = -3953555937425951602L;
    @SerializedName("sucess")
    @Expose
    private Boolean sucess;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSucess() {
        return sucess;
    }

    public void setSucess(Boolean sucess) {
        this.sucess = sucess;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

}
