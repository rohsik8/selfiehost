package com.selfie.host.ui.fragments.properties.mvp;

import android.app.ProgressDialog;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.selfie.host.ext.CommonResponse.CommonResponse;
import com.selfie.host.ui.activities.homepage.HomePageActivity;
import com.selfie.host.ui.fragments.properties.AddpropertyParams;
import com.selfie.host.ui.fragments.properties.propertiesResponse.Datum;
import com.selfie.host.ui.fragments.properties.propertiesResponse.PropertyResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.Timed;
import okhttp3.ResponseBody;

public class PropertyPresenter {

    private static String newsId = null;
    private final PropertyView propertyView;
    private final PropertyModel propertyModel;
    int page = 0;
    ArrayList<Datum> propertyList = new ArrayList<>();
    JSONArray myPhotosArray = new JSONArray();
    JSONArray myAminitiesArray = new JSONArray();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private LinkedHashMap<String, String> hashMap;
    private DisposableObserver<Timed<Long>> disposableObserver;


    public PropertyPresenter(PropertyView propertyView, PropertyModel propertyModel) {
        this.propertyView = propertyView;
        this.propertyModel = propertyModel;

    }

    public void onCreateView() {
        propertyList(0);
        //onDetailClick();

        propertyView.addPropertyDialog.binding.btnSave.setOnClickListener(v -> {
            addProperty();
        });
    }

    // get response list
    private void propertyList(int page) {
        // propertyView.showLoading(true);
        ProgressDialog progressDialog = new ProgressDialog(propertyView.activity);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        DisposableObserver<PropertyResponse> disposableObserver = new DisposableObserver<PropertyResponse>() {

            @Override
            public void onNext(PropertyResponse response) {

                if (response.getSucess()) {
                    progressDialog.dismiss();
                    // propertyList.addAll(response.getData());
                    /*if(response.getData()!=null&&response.getData().size()!=0) {
                        propertyList.addAll(response.getData());
                        propertyView.recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(propertyView.layoutManager()) {
                            @Override
                            public void onLoadMore(int page, int totalItemsCount) {
                                propertyList(page);
                            }
                        });
                    }*/
                    propertyView.setNewsList(response.getData());

                }
            }

            @Override
            public void onError(Throwable e) {
                progressDialog.dismiss();
                if (e instanceof HttpException) {
                    ResponseBody responseBody = ((HttpException) e).response().errorBody();
                    propertyView.showMessage(getErrorMessage(responseBody));

                } else if (e instanceof SocketTimeoutException) {
                    propertyList(0);
                } else if (e instanceof IOException) {
                    propertyView.showMessage("Please check your network connection");

                } else {
                    propertyView.showMessage(e.getMessage());
                }
            }

            @Override
            public void onComplete() {

                //  propertyView.showLoading(false);
            }
        };

        propertyModel.getPropertyObservable(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(disposableObserver);
        compositeDisposable.add(disposableObserver);
    }

/*
    //for detail view
    private void onDetailClick() {
        DisposableObserver<Datum> disposableObserver = getDetailClickObserver();
        propertyView.getDetailClickObservable().subscribe(disposableObserver);
        compositeDisposable.add(disposableObserver);

    }*/

   /* private DisposableObserver<Datum> getDetailClickObserver() {
        return new DisposableObserver<Datum>() {
            @Override
            public void onNext(Datum datumN) {
                propertyView.StartDetail(datumN.getNewsId());
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                Timber.e("Clicked");
            }
        };
    }*/


    // add property
    private void addProperty() {
        // propertyView.showLoading(true);

        ProgressDialog progressDialog = new ProgressDialog(propertyView.activity);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        DisposableObserver<CommonResponse> disposableObserver = new DisposableObserver<CommonResponse>() {

            @Override
            public void onNext(CommonResponse response) {

                propertyView.showMessage(response.getMessage());
                progressDialog.dismiss();
                propertyView.addPropertyDialog.showDialog(false);
                propertyList(0);

            }

            @Override
            public void onError(Throwable e) {
                progressDialog.dismiss();
                if (e instanceof HttpException) {
                    ResponseBody responseBody = ((HttpException) e).response().errorBody();
                    propertyView.showMessage(getErrorMessage(responseBody));

                } else if (e instanceof SocketTimeoutException) {
                    propertyList(0);
                } else if (e instanceof IOException) {
                    propertyView.showMessage("Please check your network connection");

                } else {
                    propertyView.showMessage(e.getMessage());
                }
            }

            @Override
            public void onComplete() {

                //  propertyView.showLoading(false);
            }
        };

        propertyModel.addPropertyObservable(addpropertyParams())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(disposableObserver);
        compositeDisposable.add(disposableObserver);
    }


    public AddpropertyParams addpropertyParams() {

        myAminitiesArray.put("test");
        myAminitiesArray.put("test1");

        myPhotosArray.put("");

        return AddpropertyParams.builder()

                .photos(myPhotosArray.toString())
                .titlep(propertyView.addPropertyDialog.binding.etTitle.getText().toString().isEmpty() ? "" : propertyView.addPropertyDialog.binding.etTitle.getText().toString())
                .price(propertyView.addPropertyDialog.binding.etPpn.getText().toString().isEmpty() ? "" : propertyView.addPropertyDialog.binding.etPpn.getText().toString())

                .rooms(propertyView.addPropertyDialog.binding.etNor.getText().toString().isEmpty() ? "" : propertyView.addPropertyDialog.binding.etNor.getText().toString())
                .baths(propertyView.addPropertyDialog.binding.etNob.getText().toString().isEmpty() ? "" : propertyView.addPropertyDialog.binding.etNob.getText().toString())
                .guests(propertyView.addPropertyDialog.binding.etNog.getText().toString().isEmpty() ? "" : propertyView.addPropertyDialog.binding.etNog.getText().toString())

                .desc(propertyView.addPropertyDialog.binding.etDescription.getText().toString().isEmpty() ? "" : propertyView.addPropertyDialog.binding.etDescription.getText().toString())
                .amenities(myAminitiesArray.toString())
                .lat(HomePageActivity.latitude)
                .lon(HomePageActivity.longitude).build();
    }


    public void onDestroyView() {
        compositeDisposable.clear();
    }

    //error message return
    private String getErrorMessage(ResponseBody responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody.string());
            return jsonObject.getString("message");
        } catch (Exception e) {
            return e.getMessage();
        }
    }


}
