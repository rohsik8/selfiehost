package com.selfie.host.ui.activities.login.dagger;


import com.selfie.host.application.dagger.AppComponent;
import com.selfie.host.ui.activities.login.LoginActivity;

import dagger.Component;

@LoginScope
@Component(modules = LoginModule.class, dependencies = AppComponent.class)
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
}
