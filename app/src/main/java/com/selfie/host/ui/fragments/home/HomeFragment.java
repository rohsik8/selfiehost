package com.selfie.host.ui.fragments.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.selfie.host.application.dagger.AppApplication;
import com.selfie.host.ui.fragments.home.dagger.DaggerHomeComponent;
import com.selfie.host.ui.fragments.home.dagger.HomeModule;
import com.selfie.host.ui.fragments.home.mvp.HomePresenter;
import com.selfie.host.ui.fragments.home.mvp.HomeView;

import javax.inject.Inject;


public class HomeFragment extends Fragment {
    public static boolean resume = true;
    @Inject
    HomePresenter homePresenter;
    @Inject
    HomeView homeView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        DaggerHomeComponent.builder()
                .appComponent(AppApplication.get(getActivity()).appComponent())
                .homeModule(new HomeModule((AppCompatActivity) getActivity()))
                .build()
                .inject(this);


        homePresenter.onCreateView();


        return homeView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }

    @Override
    public void onResume() {
        super.onResume();


    }
}
