package com.selfie.host.ui.fragments.properties.dagger;


import javax.inject.Scope;

@Scope
public @interface PropertyScope {
}
