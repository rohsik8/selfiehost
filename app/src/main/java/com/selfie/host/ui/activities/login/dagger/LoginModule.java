package com.selfie.host.ui.activities.login.dagger;

import android.support.v7.app.AppCompatActivity;

import com.selfie.host.application.AppNetwork;
import com.selfie.host.ext.storage.PreferencesManager;
import com.selfie.host.ui.activities.login.mvp.LoginModel;
import com.selfie.host.ui.activities.login.mvp.LoginPresenter;
import com.selfie.host.ui.activities.login.mvp.LoginView;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

    private final AppCompatActivity activity;

    public LoginModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @LoginScope
    @Provides
    public LoginView loginView(PreferencesManager preferencesManager) {
        return new LoginView(activity, preferencesManager);
    }

    @LoginScope
    @Provides
    public LoginModel loginModel(AppNetwork appNetwork, PreferencesManager preferencesManager) {
        return new LoginModel(appNetwork, preferencesManager);
    }

    @LoginScope
    @Provides
    public LoginPresenter loginPresenter(LoginView loginView, LoginModel loginModel) {
        return new LoginPresenter(loginView, loginModel);
    }
}
