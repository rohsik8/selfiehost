package com.selfie.host.ui.fragments.properties.dagger;


import com.selfie.host.application.dagger.AppComponent;
import com.selfie.host.ui.fragments.properties.PropertyListFragment;

import dagger.Component;

@PropertyScope
@Component(modules = PropertyModule.class, dependencies = AppComponent.class)
public interface PropertyComponent {

    void inject(PropertyListFragment fragment);
}
