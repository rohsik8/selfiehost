package com.selfie.host.ui.activities.login.mvp;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.selfie.host.R;
import com.selfie.host.databinding.LoginLayoutBinding;
import com.selfie.host.ext.storage.PreferencesManager;
import com.selfie.host.ui.activities.register.RegisterActivity;

public class LoginView extends FrameLayout {


    public Activity mActivity;
    PreferencesManager sharedPreferenceManager;
    LoginLayoutBinding binding;

    public LoginView(@NonNull AppCompatActivity activity, PreferencesManager preferencesManager) {
        super(activity);
        this.mActivity = activity;
        this.sharedPreferenceManager = preferencesManager;
        binding = DataBindingUtil.setContentView(activity, R.layout.login_layout);

       /* binding.btnLogin.setOnClickListener(v->{
            activity.startActivity(new Intent(activity, HomePageActivity.class));
            activity.finish();
        });*/

        binding.tvRegister.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, RegisterActivity.class));
            activity.finish();
        });

    }

    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    public void showLoading(boolean isLoading) {
        if (isLoading) {
            binding.include.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        } else {
            binding.include.findViewById(R.id.loading).setVisibility(View.GONE);
        }

    }

}




