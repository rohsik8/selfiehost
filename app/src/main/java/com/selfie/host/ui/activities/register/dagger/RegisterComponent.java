package com.selfie.host.ui.activities.register.dagger;


import com.selfie.host.application.dagger.AppComponent;
import com.selfie.host.ui.activities.register.RegisterActivity;

import dagger.Component;

@RegisterScope
@Component(modules = RegisterModule.class, dependencies = AppComponent.class)
public interface RegisterComponent {
    void inject(RegisterActivity registerActivity);
}
