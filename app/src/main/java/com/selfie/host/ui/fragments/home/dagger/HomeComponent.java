package com.selfie.host.ui.fragments.home.dagger;


import com.selfie.host.application.dagger.AppComponent;
import com.selfie.host.ui.fragments.home.HomeFragment;

import dagger.Component;

@HomeScope
@Component(modules = HomeModule.class, dependencies = AppComponent.class)
public interface HomeComponent {

    void inject(HomeFragment homeFragment);
}
