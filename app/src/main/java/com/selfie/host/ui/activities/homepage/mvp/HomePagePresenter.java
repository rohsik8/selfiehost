package com.selfie.host.ui.activities.homepage.mvp;

import android.graphics.PorterDuff;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;

import com.selfie.host.R;
import com.selfie.host.ext.storage.PreferencesManager;

import io.reactivex.disposables.CompositeDisposable;


public class HomePagePresenter {

    private final HomePageView homePageView;
    private final HomePageModel homePageModel;
    PreferencesManager preferencesManager;


    boolean logout = true;
    private String count = "0";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    public HomePagePresenter(HomePageView homePageView, HomePageModel homePageModel, PreferencesManager preferencesManager) {
        this.homePageView = homePageView;
        this.homePageModel = homePageModel;
        this.preferencesManager = preferencesManager;
    }

    public void onCreate() {
        talSelection();

    }

    private void setFragment(int position) {

        homePageView.setCurrentTabFragment(position);

    }

    private void talSelection() {

        homePageView.allTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                homePageView.selectedItemPosition = tab.getPosition();
                setFragment(tab.getPosition());
                if (tab.getIcon() != null) {
                    int tabIconColor = ContextCompat.getColor(homePageView.activity, R.color.colorAccent);
                    tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getIcon() != null) {
                    int tabIconColor = ContextCompat.getColor(homePageView.activity, R.color.colorDGreyG);
                    tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getIcon() != null) {
                    if (!tab.isSelected()) {
                        int tabIconColor = ContextCompat.getColor(homePageView.activity, R.color.colorDGreyG);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    } else {
                        if (homePageView.selectedItemPosition == 2) {
                            int tabIconColor = ContextCompat.getColor(homePageView.activity, R.color.colorAccent);
                            tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                            setFragment(tab.getPosition());
                        } else {
                            setFragment(tab.getPosition());

                        }

                    }

                }
            }
        });

    }

    public void onDestroyView() {
        compositeDisposable.clear();
    }


}
