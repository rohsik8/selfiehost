package com.selfie.host.application.dagger.modules;


import com.fatboyindustrial.gsonjodatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.selfie.host.application.dagger.AppScope;
import com.selfie.host.utils.AppTypeAdapterFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class GsonModule {

    @Provides
    @AppScope
    public Gson gson() {
        return Converters.registerAll(new GsonBuilder())
                .registerTypeAdapterFactory(AppTypeAdapterFactory.create())
                .create();
    }
}
