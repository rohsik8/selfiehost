package com.selfie.host.application.dagger;


import javax.inject.Scope;

@Scope
public @interface AppScope {
}
