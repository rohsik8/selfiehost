package com.selfie.host.application.dagger;


import android.content.Context;

import com.selfie.host.application.AppNetwork;
import com.selfie.host.application.dagger.modules.AppModule;
import com.selfie.host.application.dagger.modules.GsonModule;
import com.selfie.host.application.dagger.modules.NetworkModule;
import com.selfie.host.ext.storage.PreferencesManager;
import com.squareup.picasso.Picasso;

import dagger.Component;
import okhttp3.OkHttpClient;

@AppScope
@Component(modules = {AppModule.class, GsonModule.class, NetworkModule.class})
public interface AppComponent {

    Context context();

    OkHttpClient okHttpClient();

    AppNetwork vepNetwork();


    PreferencesManager preferencesManager();


    Picasso picasso();

}
