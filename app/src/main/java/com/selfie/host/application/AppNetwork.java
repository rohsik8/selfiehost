package com.selfie.host.application;


import com.selfie.host.ext.CommonResponse.CommonResponse;
import com.selfie.host.ui.activities.login.LoginParams;
import com.selfie.host.ui.activities.login.LoginResponse.LoginResponse;
import com.selfie.host.ui.activities.register.RegisterParams;
import com.selfie.host.ui.activities.register.RegisterResponse.RegisterResponse;
import com.selfie.host.ui.fragments.properties.AddpropertyParams;
import com.selfie.host.ui.fragments.properties.propertiesResponse.PropertyResponse;
import com.selfie.host.utils.Constants;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;


public interface AppNetwork {

    @POST(Constants.LOGIN)
    Observable<LoginResponse> loginResponse(@Body LoginParams loginParams);

    @POST(Constants.REGISTER)
    Observable<RegisterResponse> registerResponse(@Body RegisterParams registerParams);


    @GET
    Observable<PropertyResponse> getAllProperty(@Url String url, @Header("Authorization") String token);

    @POST(Constants.PROPERTYADD)
    Observable<CommonResponse> addProperty(@Header("Authorization") String token, @Body AddpropertyParams addpropertyParams);


}
