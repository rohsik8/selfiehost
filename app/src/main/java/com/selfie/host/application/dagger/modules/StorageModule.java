package com.selfie.host.application.dagger.modules;


import android.content.Context;

import com.selfie.host.application.dagger.AppScope;
import com.selfie.host.ext.storage.PreferencesManager;

import dagger.Module;
import dagger.Provides;

/**/

@Module
public class StorageModule {

    @AppScope
    @Provides
    public PreferencesManager preferencesManager(Context context) {
        return new PreferencesManager(context);
    }
}
